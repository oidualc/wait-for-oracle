package std;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class App {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, InterruptedException {
        final int MAX_RETRIES = 30;
        final int DELAY = 5000;

        final String DB_HOST = System.getenv("DB_HOST");
        final String DB_PORT = System.getenv("DB_PORT");
        final String DB_NAME = System.getenv("DB_NAME");
        final String DB_USERNAME = System.getenv("DB_USERNAME");
        final String DB_PASSWORD = System.getenv("DB_PASSWORD");
        final String DB_TEST_TABLE = System.getenv("DB_TEST_TABLE");

        final String DB_JDBC_URL = "jdbc:oracle:thin:@" + DB_HOST + ":"+ DB_PORT + "/" + DB_NAME;

        if (DB_HOST == null)
            throw new RuntimeException("DB_HOST is null, please specify it as environment variable");
        if (DB_PORT == null)
            throw new RuntimeException("DB_PORT is null, please specify it as environment variable");
        if (DB_NAME == null)
            throw new RuntimeException("DB_NAME is null, please specify it as environment variable");
        if (DB_USERNAME == null)
            throw new RuntimeException("DB_USERNAME is null, please specify it as environment variable");
        if (DB_PASSWORD == null)
            throw new RuntimeException("DB_PASSWORD is null, please specify it as environment variable");

        Class.forName("oracle.jdbc.driver.OracleDriver");

        for (int i=0; i<MAX_RETRIES; i++) {
            try {
                Connection con=DriverManager.getConnection(DB_JDBC_URL, DB_USERNAME, DB_PASSWORD);
                Statement stmt = con.createStatement();

                stmt.executeQuery("select 1 from " + (DB_TEST_TABLE != null ? DB_TEST_TABLE : "dual"));

                System.out.println("Database is ready, exiting now");
                return;
            } catch(SQLException e) {
                System.out.println("Database is not ready yet, waiting " + DELAY / 1000 + " seconds...");
                Thread.sleep(DELAY);
            }
        }

        System.out.println("Tried " + MAX_RETRIES + " times unsuccessfully, exiting now");
        System.exit(1);
    }
}
