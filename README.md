## Installation
Build jar library with
```
./gradlew shadowJar
```

## Usage
Specify the following environment variables to wait for the oracle db:
- DB_HOST
- DB_PORT
- DB_NAME
- DB_USERNAME
- DB_PASSWORD
- DB_TEST_TABLE (optional, if not specified defaults to DUAL)

Use in a docker environment like this:
``` docker
CMD sh -c "java -jar ./wait-for-oracle.jar && my-docker-command.sh"
```